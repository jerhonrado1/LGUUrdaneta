<?php
@session_start();
include "config/dbconnect.php";
include "config/pdoConfig.php";
include "config/dbconfig.php";

if(@$_SESSION['admin']){
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>User Management | LGU Urdaneta City</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <link rel="shortcut icon" href="img/favicon.png">
        
        <script type="text/javascript">
        $(document).ready(function() {
            $('#employee_data').DataTable({
                "deferRender": true
            });
        });
        </script>

        <!-- Bootstrap 3.3.6 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="assets/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="assets/dist/js/demo.js"></script>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <!-- sidebar-collapse -->
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>CSC</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            
            <?php
                $id = $_SESSION['id'];

                $showSql = "SELECT * FROM tbl_user WHERE id = '$id'";
                $showResult = mysqli_query($con, $showSql);

                if ($showResult) {
                    while ($row = mysqli_fetch_array($showResult)) {
                        $fullname =  $row['fullname'];
                        $photo =  $row['photo'];
                        $position =  $row['position'];
                    }
                }
            ?>
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- Messages: style can be found in dropdown.less-->
                                <!-- User Account: style can be found in dropdown.less -->
                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i></a>
                                    <ul class="dropdown-menu">
                                        <!-- User image -->
                                        <li class="user-header">
                                            <img src="uploads/<?php echo $photo; ?>" class="img-circle" alt="User Image">
                                            <p>
                                                <?php echo $fullname; ?>
                                                <small><?php echo $position; ?></small>
                                            </p>
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="config/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                </nav>
            </header>


            <!-- SIDEBAR NAVIGATION MENU AND LOGO -->
            <?php $page = 'usermgmt'; include('navigation.php'); ?>
            <!-- END SIDEBAR -->
           

            <!-- Content Wrapper. Contains page content -->
            

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Add User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Full Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Full Name">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Username</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter E-mail">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>

                <div class="form-group">
                    <label for="exampleInputText">Position</label>
                    <select class="form-control">
                        <option selected="true" value="" disabled="disabled">Choose Position</option>
                        <option>Administrator</option>
                        <option>Editor</option>
                        <option>Encoder</option>
                    </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">User Image</label>
                  <input type="file" id="exampleInputFile">
                </div>
                <hr>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Wag kang kikilos ng <a href="javascript:void(0);">masama</a>.
                  </label>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-success" style="width:100%;">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-8">
        
            <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">List of Users</h3>
                </div>



            <form id="defaultForm" method="post" enctype="multipart/form-data">
                 <div class="box-body">
                    <div id="order_table">
                        <table id="employee_data" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>FULL NAME</th>
                                    <th>USERNAME</th>
                                    <th>ROLE</th>
                                </tr>
                            </thead>
                            <?php 
                            $sql = "SELECT * FROM tbl_user";
                            $result = mysqli_query($con, $sql);
                            while($row = mysqli_fetch_array($result)){
                                $id = $row['id'];
                            ?>

                            <tbody>
                                <tr>
                                    <td><?php echo $id;?> </td>
                                    <td><?php echo $row['fullname'];?></td>
                                    <td><?php echo $row['username'];?></td>
                                    <td><?php echo $row['position'];?></td>
                                </tr>

                            </tbody>

                            <?php } ?>

                        </table>
                    </div>
                </div>
            </form>
               
            </div>

        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>








            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.3.6
                </div>
                <strong>Copyright &copy; 2017-2018 <a href="#">Civil Service Commission (CSCUFO)</a>.</strong> All rights reserved.
            </footer>
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </body>

    </html>
    <?php
  }else{
    header("location: index.php");
  }
?>