<legend>
    <h1>Personal Data Sheet (Page 2)</h1></legend>
<div class="row">
    <div class="col-lg-12">
        <div class="box-body table-responsive no-padding">
            <div class="col-lg-12">
                <h3>IV. SERVICE ELIGILITY</h3>
            </div>
            <!-- Table COL 4 -->
            <div class="col-lg-12" style="font-size: 11px">
                
			


			<!-- FOR TABLE NAME OF CHILD-->
            <script type="text/javascript">
            $(function() {
                $('#addEligibility').click(function() {
                    addnewrow();
                });

                $('body').delegate('.remove', 'click', function() {
                    $(this).parent().parent().remove();
                });

            });


            function addnewrow() {
                var n = ($('.detail tr').length - 0) + 1;
                var tr = '<tr>' +
                    '<td><input type="text" class="form-control child_name" name="child_name[]"></td>' +
                    '<td><input type="date" class="form-control child_dob" name="child_dob[]"></td>' +
                    '<td><button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button></td>' +
                    '</tr>';
                $('.detail').append(tr);
            }
            </script>


            <!-- End of COL 5 -->
            <div class="col-lg-12 table-responsive">
                <table class="table table-bordered" style="font-size: 12px;">
                    <thead>
                        <tr>
                            <td rowspan="2" width="30%">27. CAREER SERVICE RA 1080 (BOARD BAR) UNDER SPECIAL LAWS/ CES/ CSEE/ BRGY ELIGILITY/ DRIVERS LICENSE</td>
                            <td rowspan="2" width="5%">RATING (If Applicable)</td>
                            <td rowspan="2" width="25%">DATE OF EXAMINATION / CONFERMENT</td>
                            <td rowspan="2" width="25%">PLACE OF EXAMINATION / CONFERMENT</td>
                            <td colspan="2" width="15%">LICENSE (If Applicable)</td>
                             <td rowspan="2">
                                <button type="button" id="addEligibility" class="btn btn-primary">
                                    <i class="fa fa-plus-square"></i>
                                </button>
                            </td>
                        </tr>

                        <tr>
	                        <td width="100">from</td>
	                        <td width="100">to</td>
	                    </tr>
                    </thead>
                    <tbody class="detail">
                        <tr>
                            <td rowspan="2">
                                <input type="text" id="textbox9" class="form-control child_name" name="cs_eligbility[]">
                            </td>
                            <td rowspan="2">
                                <input type="text" id="textbox9" class="form-control child_dob" name="cs_rating[]">
                            </td>
                            <td rowspan="2">
                                <input type="date" id="textbox9" class="form-control child_dob" name="cs_date[]">
                            </td>
                            <td colspan="2">
                                <input type="text" id="textbox9" class="form-control child_dob" name="cs_place[]">
                            </td>
                            
                       
                            <td>
                                <input type="date" id="textbox9" class="form-control child_dob" name="cs_from[]">
                            </td>
                            <td>
                                <input type="date" id="textbox9" class="form-control child_dob" name="cs_to[]">
                            </td>

                            <td>
                                <button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>




            </div>
        </div>
    </div>
</div>