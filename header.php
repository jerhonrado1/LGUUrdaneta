
<?php
@session_start();
include "config/dbconnect.php";
include "config/pdoConfig.php";

if(@$_SESSION['Admin']){
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OSDS | Add Personnel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="assets/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
  <!-- demo style -->

   <!--For validation -->
   <link rel="stylesheet" href="assets/validation2/dist/css/bootstrapValidator.css"/>
   <script src="assets/validation2/vendor/jquery/jquery-1.10.2.min.js"></script>
   <script src="assets/validation2/dist/js/bootstrapValidator.js"></script>

  <!-- ui jquery -->
  <link rel="stylesheet" type="text/css" href="assets/jquery-ui-1.12.1/jquery-ui.min.css">
  <link rel="stylesheet" type="text/css" href="assets/jquery-ui-1.12.1/jquery-ui.css">
  <script type="text/javascript" src="../assets/jquery-ui-1.12.1/jquery-ui.js"></script>

  <!-- Custom Fonts -->
  <link rel="stylesheet" type="text/css" href="assets/asset/font-awesome-4.6.3/css/font-awesome.min.css">

  <link href='assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>

  <!-- Ionicons -->
  <link rel="stylesheet" type="text/css" href="assets/ionicons-2.0.1/css/ionicons.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="assets/plugins/datatables/dataTables.bootstrap.css">
  
  <link rel="shortcut icon" href="img/favicon.png">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- sidebar-collapse -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>LGU</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>LGU-Urdaneta</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          
          <?php
                $id = $_SESSION['id'];

                $showSql = "SELECT * FROM user WHERE id = '$id'";
                $showResult = mysqli_query($con, $showSql);

                if ($showResult) {
                    while ($row = mysqli_fetch_array($showResult)) {
                        $fullname =  $row['fullname'];
                        $photo =  $row['photo'];
                        $position =  $row['position'];
                    }
                }
            ?>
          
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i></a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="images/avatar5.png" class="img-circle" alt="User Image">

                <p>
                    <?php echo $fullname; ?>
                    <small><?php echo $position; ?></small>
                </p>
              </li>

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>






  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="images/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
        </div>
      </div>

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview active">
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-suitcase"></i>
            <span>Master Files</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
          </a>
          <ul class="treeview-menu">
              <li class=""><a href="masterfiles_division.php"><i class="fa fa-circle-o"></i> Division Personnel</a>
              <li class=""><a href="masterfiles_elementary.php"><i class="fa fa-circle-o"></i> Elementary Level</a></li>
              <li class=""><a href="masterfiles_secondary.php"><i class="fa fa-circle-o"></i> Secondary Level</a>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-search"></i>
            <span>Search By</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="search_appointment.php"><i class="fa fa-calendar"></i> Original Appointment</a></li>
            <li><a href="search_promotion.php"><i class="fa fa-line-chart"></i> Last Promotion</a></li>
            <li class="">
              <a href="#"><i class="fa fa-graduation-cap"></i> Educational/Degree
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class=""><a href="search_college.php"><i class="fa fa-circle-o"></i> College</a></li>
                <li class=""><a href="search_graduate.php"><i class="fa fa-circle-o"></i> Graduate Studies</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="add_personnel.php">
            <i class="fa fa-user-plus"></i> <span>Add Personnel</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-plus-square"></i><span> Another Entry</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="position.php"><i class="fa fa-circle-o"></i> Position</a></li>
            <li class=""><a href="school.php"><i class="fa fa-circle-o"></i> School</a>
          </ul>
        </li>
      </ul>
    </section>
  </aside>

<?php
  }else{
    header("location: index.php");
  }
?>