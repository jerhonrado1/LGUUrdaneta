<?php
@session_start();
include "config/dbconnect.php";
include "config/pdoConfig.php";
include "config/dbconfig.php";

if(@$_SESSION['admin']){
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VIEW APPOINTEES | CSCUFO</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

  <!-- Custom Fonts -->
  <link rel="stylesheet" type="text/css" href="assets/asset/font-awesome-4.6.3/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/js/jquery.dataTables.min.css"/>
  <script src="assets/dist/js/jquery-1.11.3.min.js"></script>
  <script src="assets/dist/js/jquery.dataTables.min.js"></script>
  
  <link rel="shortcut icon" href="img/favicon.png">

  <script type="text/javascript">
    $(document).ready(function() {
        $('#employee_data').DataTable( {
            "deferRender": true
        } );
    } );
  </script>


<!-- Bootstrap 3.3.6 -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/dist/js/demo.js"></script>



</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- sidebar-collapse -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CSC</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            
            <?php
                $id = $_SESSION['id'];

                $showSql = "SELECT * FROM tbl_user WHERE id = '$id'";
                $showResult = mysqli_query($con, $showSql);

                if ($showResult) {
                    while ($row = mysqli_fetch_array($showResult)) {
                        $fullname =  $row['fullname'];
                        $photo =  $row['photo'];
                        $position =  $row['position'];
                    }
                }
            ?>
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- Messages: style can be found in dropdown.less-->
                                <!-- User Account: style can be found in dropdown.less -->
                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i></a>
                                    <ul class="dropdown-menu">
                                        <!-- User image -->
                                        <li class="user-header">
                                            <img src="uploads/<?php echo $photo; ?>" class="img-circle" alt="User Image">
                                            <p>
                                                <?php echo $fullname; ?>
                                                <small><?php echo $position; ?></small>
                                            </p>
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="config/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                </nav>
            </header>


            <!-- SIDEBAR NAVIGATION MENU AND LOGO -->
            <?php $page = 'masterfiles'; include('navigation.php'); ?>
            <!-- END SIDEBAR -->
           

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
        <i class="fa fa-search"></i> VIEW ALL APPOINTEES
      </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">UI</a></li>
                        <li class="active">Icons</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="defaultForm" method="post" enctype="multipart/form-data">
                                <div class="nav-tabs-custom">
                                    <div class="tab-content">
                                        <!-- Page 1 -->
                                        <div class="tab-pane active">
                                            <div class="box-body">
                                                <h1>VIEW ALL RECORDS OF APPOINTEES</h1>
                                                <div id="order_table">
                                                    <table id="employee_data" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">ID</th>
                                                                <th class="text-center" width="20%">FULL NAME</th>
                                                                <th class="text-center">POSITION</th>
                                                                <th class="text-center">NATURE</th>
                                                                <th class="text-center">AGENCY</th>
                                                                <th class="text-center">STATUS</th>
                                                                <th width="20%" class="text-center">DATE TIME RECEIVED</th>
                                                                <th width="20%" class="text-center">DATE TIME SIGNED</th>
                                                                <th>TIME SPENT</th>
                                                                <th width="10%">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- /.nav-tabs-custom -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.3.6
                </div>
                <strong>Copyright &copy; 2017-2018 <a href="#">Civil Service Commission (CSCUFO)</a>.</strong> All rights reserved.
            </footer>
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </body>

    </html>
    <?php
  }else{
    header("location: index.php");
  }
?>